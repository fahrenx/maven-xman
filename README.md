Instructions:

Go to `maven-xman` directory

run `build.sh`
and then run.sh with arguments - path to your pom file and optionally to your settings.xml, for example:
`./run.sh ../../missiles-launch-controller/ /home/johndoe/.m2/settings.xml`

wait a bit and you should get nice html report about your dependencies, like this:

It clearly says which dependencies you declared, which are inherited, how many of them you have. You can also sort them by each column. Maven dependency:analyze report is also attached (http://maven.apache.org/plugins/maven-dependency-plugin/analyze-mojo.html).

If you have graphviz installed and use maven dependency plugin version higher then 2.1 you will also get a graph of your dependencies.

But wait, there is more - hover over the number of conflicts detected and you will get the relevant subtree presenting dependency conflicts for that given artifact.

Another nice thing is that if you have multiple maven modules nested in a given folder, a tool will recursively traverse your directory looking for pom files and generate report for each of them. You will get a report like this.

The tool internally is simple, it actually parses data from mvn dependency:tree and few other maven plugin commands - brutally on the text level. Two reasons for that - writing Maven plugins is not a fun so I reject that idea quickly and the fact that results are consistent with what you get on command line is also a plus. It is written in Groovy (which was an adventure on its own) and apparently made of magic code substance called “unmaintanium” but it should get better.