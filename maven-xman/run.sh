#!/bin/bash
maven_path=`which mvn`
dot_path=`which dot`
echo "maven found: ${maven_path}"
echo "dot found: ${dot_path}"
if [ $# -eq 0 ]
  then
    echo "No arguments supplied - will analyse project in current directory and with default settings.xml"
    echo "usage: ./run.sh [path_to_pom_file] [path_to_maven_settings_xml]"
    java -Dmaven_path=$maven_path -Ddot_path=$dot_path -jar build/distributions/maven-xman-unspecified-shadow.jar .
else
    java -Dmaven_path=$maven_path -Ddot_path=$dot_path -jar build/distributions/maven-xman-unspecified-shadow.jar $1 $2
fi


