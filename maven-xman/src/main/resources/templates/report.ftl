<html>
<head>
    <title>X-Man report for ${projectName}</title>

    <link rel="stylesheet" href="css/main.css" type="text/css"/>
    <link href="css/tipr_mod.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-latest.js"></script>
    <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
    <script src="js/tipr.js"></script>

    <script type="text/javascript" id="js">
        $(document).ready(function() {
            $("#declaredDepsTable").tablesorter();
            $("#undeclaredDepsTable").tablesorter();
            $("td.tree").tipr();

            $("td.tree").each(function() {
                counter = $(this).attr("data-counter")
                console.log(counter)
                if(counter==1) {
                    $(this).css("color","green")
                }
                else if(counter >1){
                    $(this).css("color","red")
                }
            });
        });

    </script>
</head>

<body>
<div id="banner">
    <h1>Maven<em>X-Man</em></h1>
    <h3>Take control over your dependencies</h3>
    <a href="#"></a>
</div>

<h2 style="padding-bottom: 30px;padding-top: 30px" >Maven dependencies report for project: '${projectName}'</h2>

<h3 class="beforeTableHeader">Dependencies declared in pom (${numOfDeclared}):</h3>

<table id="declaredDepsTable" class="depsTable" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
        <th><span>full name</span></th>
        <th><span>groupId</span></th>
        <th><span>artifactId</span></th>
        <th><span>version</span></th>
        <th><span>inherited?</span></th>
        <th><span>can be updated?</span></th>
        <th><span># of candidates for resolution</span></th>
    </tr>
    </thead>
    <tbody>
        <#list dependencies as dep>

            <#if dep.declared==true>
            <tr>
                <td>${dep.artifactIdentifiers.groupId}:${dep.artifactIdentifiers.artifactId}:${dep.artifactIdentifiers.version}</td>
                <td>${dep.artifactIdentifiers.groupId}</td>
                <td>${dep.artifactIdentifiers.artifactId}</td>
                <td>${dep.artifactIdentifiers.version}</td>
                <td>${dep.inherited}</td>
                <#if dep.updatable>
                    <td>yes - latest: ${dep.latestVersion}</td>
                <#else>
                    <td>no</td>
                </#if>
                <td class="tree" data-tip="${dep.resolutionTree}" data-counter="${dep.numberOfCandidates}">${dep.numberOfCandidates}</td>
            </tr>
            </#if>

        </#list>
    </tbody>

</table>

<h3 class="beforeTableHeader">Transitive and inherited dependencies (${numOfUndeclared}):</h3>
<table id="undeclaredDepsTable" class="depsTable"  cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th><span>full name</span></th>
            <th><span>groupId</span></th>
            <th><span>artifactId</span></th>
            <th><span>version</span></th>
            <th><span>inherited?</span></th>
            <th><span>can be updated?</span></th>
            <th><span># of candidates for resolution</span></th>
        </tr>
    </thead>
    <tbody>
            <#list dependencies as dep>

                <#if dep.declared==false>
                <tr>
                    <td>${dep.artifactIdentifiers.groupId}:${dep.artifactIdentifiers.artifactId}:${dep.artifactIdentifiers.version}</td>
                    <td>${dep.artifactIdentifiers.groupId}</td>
                    <td>${dep.artifactIdentifiers.artifactId}</td>
                    <td>${dep.artifactIdentifiers.version}</td>
                    <td>${dep.inherited}</td>
                    <#if dep.updatable>
                        <td>yes - latest: ${dep.latestVersion}</td>
                    <#else>
                        <td>no</td>
                    </#if>
                    <td class="tree" data-tip="${dep.resolutionTree}" data-counter="${dep.numberOfCandidates}">${dep.numberOfCandidates}</td>
                </tr>
                </#if>

            </#list>
    </tbody>

</table>


<h3>Total number of dependencies (declared, inherited and transitive): ${dependencies?size}</h3>
<h4>Dependency usage report (depenency-analyze):</h4>
<a href="${dependencyUsageReportLink}" >dependency-usage-report</a>

<h4>Graph</h4>

<a href ="${graphFilename}">
    <img src="${graphFilename}" title="open graph" style="max-width:100%; max-height:100%;">
</a>
</body>

</html>
