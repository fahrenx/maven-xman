package pl.tl.mavenxman

import freemarker.template.Configuration
import freemarker.template.Template
import groovy.io.FileType
import groovy.transform.ToString
import org.apache.commons.exec.ExecuteException
import org.apache.commons.io.FileUtils
import pl.tl.mavenxman.cmdline.CmdLineExecutor
import pl.tl.mavenxman.graph.DependencyResolutionGraph
import pl.tl.mavenxman.graph.DependencyResolutionGraphPrinter
import pl.tl.mavenxman.graph.TextDependencyTreeParser
import pl.tl.mavenxman.model.ArtifactGroupAndId
import pl.tl.mavenxman.model.ArtifactIdentifiers
import pl.tl.mavenxman.model.DependencyTreeArtifact

import java.util.jar.JarEntry
import java.util.jar.JarFile

if(args.length==0){
    println("Not enough arguments! See usage:")
    println("XmanRunner path_to_pom_file [path_to_maven_settings_xml] \n"+
    "additional options:"+
    "\n\t-D${SystemConfig.DOT_SYSTEM_VAR} - location of 'dot'"+
    "\n\t-D${SystemConfig.MAVEN_SYSTEM_VAR} - location of maven"
    )
    println "Aborting..."
    return 1;
}

println "arguments:" + this.args

systemConfig = new SystemConfig(args.length>1?args[1]:null)
currentRunningDir = new File(".").getCanonicalPath()

//---- fail-fast
println "checking maven..."
println "${systemConfig.mavenAbsoluteName}  -version".execute().text
//TODO check more, break if fails

//----
def mavenProjectsAbsoluteNames = new HashSet()
def dir = new File(args[0])
dir.eachFileRecurse (FileType.FILES) { file ->
    if(file.getName() == "pom.xml"){
        mavenProjectsAbsoluteNames << file.getParent()
    }
}

if(mavenProjectsAbsoluteNames.isEmpty()){
    println "no projects found!"
    return 1
}
else{
    println "following projects found " + mavenProjectsAbsoluteNames
}

if(mavenProjectsAbsoluteNames.size()==1){
    def mavenProjectAbsoluteName = mavenProjectsAbsoluteNames.iterator().next();
    def conf = new Config(systemConfig, mavenProjectAbsoluteName , "${currentRunningDir}/dep-${new File(mavenProjectAbsoluteName).getName()}")
    createReportForProject(conf)
}
else{
    projectsAndReportDirs = [:]
    mavenProjectsAbsoluteNames.each{
        projectsAndReportDirs[it]="${currentRunningDir}/dep-${new File(it).getName()}"
    }
    projectsAndReportDirs.each { projectName, reportDir ->
        def conf = new Config(systemConfig, projectName, reportDir)
        createReportForProject(conf)
    }
    println "Creating aggregated report"
    createAggregatingReport(projectsAndReportDirs, "${currentRunningDir}/dep-aggregated-report.html")
}

println "maven-xman: bye"

class ReportView{
    final projectName;
    final link;
    public ReportView(projectName, link){
        this.projectName=projectName
        this.link=link
    }
}

def createAggregatingReport(projectsAndReportDirs, reportFileAbsoluteName){
    Map<String, Object> data = new HashMap<String, Object>();
    reports = []
    projectsAndReportDirs.each{ projectName, reportDir ->
        reports << new ReportView(projectName, "${new File(reportDir).getName()}/dep-report.html")
    }
    data["reports"] = reports
    generateHtml(data,"aggregation.ftl", reportFileAbsoluteName)
}


def createReportForProject(Config config){

    //preparing workspace
    println "removing old temporary files if present"
    println config.getCleanCommand().execute().text
    println "analyzing dependencies for project"+config.projectDirAbsoluteName
    println "creating report dir..."
    println config.getRemoveOldReportDirCommand().execute().text
    println config.getCreateReportDirCommand().execute().text

    //gathering data
    println "calculating dependencies..."
    println config.getVerboseDependencyTreeTxtCommand().execute().text
    DependencyResolutionGraph graph = new TextDependencyTreeParser().parse(new File(config.txtOutputAbsoluteName).text);

    //checking if dependencies can be updated
    println "checking dependency updates..."
    int timeoutInSecs = 15;
    Map<String,String> updatableDeps = [:]
    try{
        depUpdatesReport = new CmdLineExecutor().runWithTimeout(config.getDisplayDependencyUpdatesCommand(), timeoutInSecs)
        updatableDeps = extractUpdatableDepsFromReport(depUpdatesReport)
        println("Found ${updatableDeps.size()} updatable dependencies")
    }catch(ExecuteException e){
        System.err.println("Cannot retrieve data about updatable dependencies - " + e);
    }
    //---

    println "find dependencies declared in pom"
    def Set<String> depNamesDeclaredInPom = findDependenciesDeclaredInPom(config)
    println "find inherited dependencies"
    def Set<String> inheritedDepNames = findInheritedDependencies(config, depNamesDeclaredInPom)
    def List<Dep> depsData = aggregateDataAboutDeps(graph, updatableDeps, depNamesDeclaredInPom, inheritedDepNames)
    println "generating dot file with dependencies tree"
    println config.getDependencyTreeDotCommand().execute().text
    //TODO it may fail silently
    println "generating graph..."
    println config.getGenerateGraphCommand().execute().text
    println "running dependency:analyze-report"
    println config.getDependencyAnalyzeReportCommand().execute().text

    //generating report
    println "copy files to report directory"
    config.getCopyAllFilesToReportDir().each { println it.execute().text }
    println "generating report..."
    printReportFreemarker(depsData, config.projectDirAbsoluteName, config.reportDirAbsoluteName)
    println "removing temporary files"

    //cleaning workspace
    println config.getCleanCommand().execute().text
    println "done, report can be foud in ${config.reportDirAbsoluteName}"

}

private findDependenciesDeclaredInPom(Config config){
    def pom=new XmlSlurper().parse(config.pomAbsoluteName)
    /**
     * we rely on the way how other tools are presenting dependency names, it can be brittle, we need structured names
     */
    def Set<String> depFullNamePrefixes = pom.dependencies.dependency.collect{it.groupId.text().trim()+":"+it.artifactId.text().trim()}
    return depFullNamePrefixes;
}

private findInheritedDependencies(Config config, Set<String> depNamesDeclaredInPom){
    println "generating effective pom..."
    println config.getEffectivePomCommand().execute().text;

    def xml=new XmlSlurper().parse(new File(config.effectivePomAbsoluteName))
    def Set<String> depFullNamePrefixesFromEffectivePom = xml.dependencies.dependency.collect{it.groupId.text().trim()+":"+it.artifactId.text().trim()}
    depFullNamePrefixesFromEffectivePom.removeAll(depNamesDeclaredInPom)
    println "INHERITED"+depFullNamePrefixesFromEffectivePom
    return depFullNamePrefixesFromEffectivePom
}

/**
 * Should not be used due to performance issues
 * @param config
 * @param dependencies
 */
@Deprecated
private findArtifactResolutionTreeForEachDependency(Config config, List<Dep> dependencies){
    def int i=0
    for(dep in dependencies){
        def split = dep.fullname.split(":")
        String artifactGroupAndName = split[0]+":"+split[1]
        String command = config.getResolutionTreeForArtifactCommand(artifactGroupAndName)
        def resolutionTreeReport =  command.execute().text
        try{
            def resolutionTree = extractDepResolutionTreeFromReportWithInclude(resolutionTreeReport)
            dep.resolutionTree = resolutionTree;
            println "Resolution tree extracted for dependency "+dep.fullname
            println resolutionTree
        }catch(IllegalArgumentException e){
            println "Cannot extract resolution tree for "+dep.fullname
        }
    }
}


@ToString
class SystemConfig{
    final static DOT_SYSTEM_VAR ="dot_path"
    final static MAVEN_SYSTEM_VAR ="maven_path"


    final dotAbsoluteName;
    final mavenAbsoluteName;
    final settingsAbsoluteName;

    public SystemConfig(settingsAbsoluteName){
        this.dotAbsoluteName = System.getProperty(DOT_SYSTEM_VAR, "dot")
        this.mavenAbsoluteName = System.getProperty(MAVEN_SYSTEM_VAR, "mvn")
        this.settingsAbsoluteName = settingsAbsoluteName
    }
}

@ToString
class Config{

    /**
     * Convention:
     * 'name' - name of the file without path
     * 'absoluteName' - full name of the file including absolute path
    **/
    final static DEP_USAGE_FILENAME = "dependency-analysis.html";
    final static TXT_OUTPUT_NAME ="temp-dep.txt"
    final static DOT_OUTPUT_NAME ="temp-dep.dot"
    final static PNG_OUTPUT_NAME ="graph-dep.png"
    final static EFFECTIVE_POM_NAME ="effective-pom.xml"

    final String  reportDirAbsoluteName
    final String  txtOutputAbsoluteName
    final String  projectDirAbsoluteName;
    final String pomAbsoluteName;
    final String effectivePomAbsoluteName;

    private final systemConfig;
    private final projectTargetDirAbsoluteName

    private final pomArg
    private final settingsXmlArg

    private final dotOutputAbsoluteName
    private final pngOutputAbsoluteName

    private final outReportDepAnalysisAbsoluteName
    private final srcDepAnalysisAbsoluteName

    public Config(systemConfig, projectDirAbsoluteName, reportDirAbsoluteName){
        // basic settings
        this.projectDirAbsoluteName=projectDirAbsoluteName
        this.projectTargetDirAbsoluteName = "${projectDirAbsoluteName}/target"
        this.reportDirAbsoluteName=reportDirAbsoluteName
        this.pomArg = "-f ${projectDirAbsoluteName}/pom.xml"
        this.pomAbsoluteName = "${projectDirAbsoluteName}/pom.xml"
        if(systemConfig.settingsAbsoluteName != null){
            this.settingsXmlArg = "-s ${systemConfig.settingsAbsoluteName}"
        }else{
            this.settingsXmlArg = ""
        }
        this. systemConfig = systemConfig

        // intermediate files
        this.txtOutputAbsoluteName = "${projectDirAbsoluteName}/${TXT_OUTPUT_NAME}"
        this.dotOutputAbsoluteName = "${projectDirAbsoluteName}/${DOT_OUTPUT_NAME}"
        this.pngOutputAbsoluteName = "${projectDirAbsoluteName}/${PNG_OUTPUT_NAME}"
        this.effectivePomAbsoluteName = "${projectDirAbsoluteName}/${EFFECTIVE_POM_NAME}"

        // report files
        this.srcDepAnalysisAbsoluteName =  "${projectTargetDirAbsoluteName}/${DEP_USAGE_FILENAME}"
        this.outReportDepAnalysisAbsoluteName = "${reportDirAbsoluteName}/${DEP_USAGE_FILENAME}"
    }

    public getRemoveOldReportDirCommand(){
        return "rm -r ${reportDirAbsoluteName}"
    }

    public getCreateReportDirCommand(){
        return "mkdir ${reportDirAbsoluteName}"
    }

    public getDependencyTreeTxtCommand(){
        return "${systemConfig.mavenAbsoluteName} dependency:tree ${this.pomArg} ${settingsXmlArg} -DoutputFile=${TXT_OUTPUT_NAME}"
    }

    public getVerboseDependencyTreeTxtCommand(){
        return "${systemConfig.mavenAbsoluteName} dependency:tree -Dverbose=true ${this.pomArg} ${settingsXmlArg} -DoutputFile=${TXT_OUTPUT_NAME}"
    }


    public getDependencyTreeDotCommand(){
        return "${systemConfig.mavenAbsoluteName} dependency:tree ${this.pomArg} ${settingsXmlArg} -DoutputType=dot  -DoutputFile=${DOT_OUTPUT_NAME}"
    }

    public getGenerateGraphCommand(){
        return "${systemConfig.dotAbsoluteName} -v -Tpng ${dotOutputAbsoluteName} -o ${pngOutputAbsoluteName}"
    }

    public getCleanCommand(){
        return "rm ${txtOutputAbsoluteName} ${dotOutputAbsoluteName} ${pngOutputAbsoluteName} ${effectivePomAbsoluteName}";
    }

    def getDependencyAnalyzeReportCommand() {
        return "${systemConfig.mavenAbsoluteName} dependency:analyze-report ${this.pomArg} ${settingsXmlArg}"
    }

    def getCopyAllFilesToReportDir() {
        return ["cp ${srcDepAnalysisAbsoluteName} ${reportDirAbsoluteName}/${DEP_USAGE_FILENAME}",
                "cp ${pngOutputAbsoluteName} ${reportDirAbsoluteName}/${PNG_OUTPUT_NAME}"]
    }

    def getDisplayDependencyUpdatesCommand() {
        return "${systemConfig.mavenAbsoluteName} versions:display-dependency-updates ${this.pomArg} ${settingsXmlArg}"
    }


    def getEffectivePomCommand(){
        return "${systemConfig.mavenAbsoluteName}  help:effective-pom ${this.pomArg} ${settingsXmlArg} -Doutput=${EFFECTIVE_POM_NAME}"
    }

    /**
     * See patern use for 'includes' here:
     * https://maven.apache.org/plugins/maven-dependency-plugin/tree-mojo.html
     **/
    def getResolutionTreeForArtifactCommand(groupIdAndArtifact){
        String includePattern = groupIdAndArtifact
        if(includePattern.endsWith(":")){
            includePattern = includePattern+"*"
        }
        else{
            includePattern = includePattern+":*"
        }
        return "${systemConfig.mavenAbsoluteName} dependency:tree ${this.pomArg} ${settingsXmlArg} -Dverbose -Dincludes=${includePattern}"
    }


}

private TreeSet createDependenciesSet(config) {
    def depTxtFile = new File(config.txtOutputAbsoluteName)
    def deps = new TreeSet()
    depTxtFile.each {
        it.toString().eachMatch(~/[a-z].*/, { deps.add(it.trim().toLowerCase()) })
    }
    deps
}

private printReportFreemarker(List<Dep >dependencies, projectName, reportDir){
    // Build the data-model
    Map<String, Object> data = new HashMap<String, Object>();
    data.put("dependencies", dependencies);
    data.put("graphFilename", Config.PNG_OUTPUT_NAME);
    data.put("dependencyUsageReportLink", Config.DEP_USAGE_FILENAME)
    data.put("projectName", projectName)

    int numberOfDeclared = dependencies.count {it.declared==true};
    data.put("numOfDeclared", numberOfDeclared);
    data.put("numOfUndeclared",dependencies.size()-numberOfDeclared)

    generateHtml(data,"report.ftl", "${reportDir}/dep-report.html")
    copyResources(reportDir,"js")
    copyResources(reportDir,"css")
    copyResources(reportDir,"img")

}



def copyResourcesNonJar(String reportDir, String resourceDirName){
    File source = new File(pl.tl.mavenxman.XmanRunner.class.getResource("/"+resourceDirName).file);
    File dest = new File("${reportDir}/${resourceDirName}");
    try {
        FileUtils.copyDirectory(source, dest);
    } catch (IOException e) {
        e.printStackTrace();
    }
}

def copyResources(String reportDir, String resourceDirName){
    JarFile jarFile = jarForClass(pl.tl.mavenxman.XmanRunner.class);
    if(jarFile!=null){
        println("Copying resources from jar...");
        copyResourcesInJar(jarFile, resourceDirName, reportDir)
    }
    else{
        println("Copying resources from regular directory...");
        copyResourcesNonJar(reportDir, resourceDirName)
    }
}

public static JarFile jarForClass(Class<?> clazz) {
    String path = "/" + clazz.getName().replace('.', '/') + ".class";
    URL jarUrl = clazz.getResource(path);
    if(jarUrl == null){
        return null;
    }
    else{
        String url = jarUrl.toString();
        int bang = url.indexOf("!");
        String JAR_URI_PREFIX = "jar:file:";
        if (url.startsWith(JAR_URI_PREFIX) && bang != -1) {
            try {
                return new JarFile(url.substring(JAR_URI_PREFIX.length(), bang));
            } catch (IOException e) {
                throw new IllegalStateException("Error loading jar file.", e);
            }
        } else {
            return null;
        }
    }
    return jarUrl;
}

/**
 * Copies a directory from a jar file to an external directory.
 */
public static void copyResourcesInJar(JarFile fromJar, String jarDir, String destDir) throws IOException {
    for (Enumeration<JarEntry> entries = fromJar.entries(); entries.hasMoreElements();) {
        JarEntry entry = entries.nextElement();
        if (entry.getName().startsWith(jarDir + "/") && !entry.isDirectory()) {
            File dest = new File(destDir + "/" +jarDir+"/"+ entry.getName().substring(jarDir.length() + 1));
            File parent = dest.getParentFile();
            if (parent != null) {
                parent.mkdirs();
            }

            FileOutputStream out = new FileOutputStream(dest);
            InputStream input = fromJar.getInputStream(entry);

            try {
                byte[] buffer = new byte[8 * 1024];

                int s = 0;
                while ((s = input.read(buffer)) > 0) {
                    out.write(buffer, 0, s);
                }
            } catch (IOException e) {
                throw new IOException("Could not copy asset from jar file", e);
            } finally {
                try {
                    input.close();
                } catch (IOException ignored) {}
                try {
                    out.close();
                } catch (IOException ignored) {}
            }
        }
    }

}




def generateHtml(model, templateName, outpuFilename){
    Configuration cfg = new Configuration();
    cfg.setClassForTemplateLoading(pl.tl.mavenxman.XmanRunner.class,"/templates/")
    Template template = cfg.getTemplate(templateName);
    Writer file = new FileWriter (new File(outpuFilename));
    template.process(model, file);
    file.flush();
    file.close();
}



private Map<String, String> extractUpdatableDepsFromReport(String report){
    int indexStartingRelevantPart = report.lastIndexOf("have newer versions");
    if(indexStartingRelevantPart == -1){
        return [:]
    }
    /* TODO can be extracted to util - it will be a common operation */
    relevantLines = report.substring(indexStartingRelevantPart).readLines()[1..-1];
    Map<String, String> depNameAndAvailableMoreRecentVersion = [:];
    for(line in relevantLines){
        if(line.contains("->")){
            matcher = ( line =~ /\[INFO\]\s+(.*) \.+ (.*) -> (.*)/ )
            depName = matcher[0][1].toString().trim()
            depLatestVersion = matcher[0][3].toString().trim()
            depNameAndAvailableMoreRecentVersion.put(depName, depLatestVersion)
        }
        else{
            break
        }

    }
    depNameAndAvailableMoreRecentVersion
}

private extractDepResolutionTreeFromReportWithInclude(String report){
    int indexStartingRelevantPart = report.indexOf("--- maven-dependency-plugin:");
    if(indexStartingRelevantPart == -1){
        throw new IllegalArgumentException("Unexpected format of the dependency:tree report for single artifact")
    }
    /* TODO can be extracted to util - it will be a common operation */
    relevantLines = report.substring(indexStartingRelevantPart).readLines()[1..-1];
    final String MARKER_OF_FIRST_LINE_AFTER_TREE = "----------"
    StringBuilder result = new StringBuilder();
    for(line in relevantLines){
        if(line.contains(MARKER_OF_FIRST_LINE_AFTER_TREE)){
            break
        }
        else{
            matcher = ( line =~ /\[INFO\](.*)/ )
            treeNode = matcher[0][1].toString()
            result.append(treeNode)
            result.append("\n");
        }
    }
    result
}

@ToString
class Dep{
    ArtifactIdentifiers artifactIdentifiers;
    String fullname
    boolean updatable=false
    String latestVersion
    String transitive =false
    String inherited;
    boolean declared;
    String unusedDeclared = false

    String resolutionTree;
    int numberOfCandidates;
}

/**
 * TODO optimize traversing
 */
private aggregateDataAboutDeps(DependencyResolutionGraph graph, Map<String,String> updatableDeps, Set<String> depNamesDeclaredInPom, Set<String> inheritedDepNames){

    def List<Dep> depsData = []
    DependencyResolutionGraphPrinter printer = new DependencyResolutionGraphPrinter();
    for(DependencyTreeArtifact depArt in graph.getResolvedDependencies()){
        String name =  depArt.fullName
        def transitive = false;
        def declared = false;
        for(declaredInPom in depNamesDeclaredInPom){
            if(name.startsWith(declaredInPom)){
                declared = true
            }
        }
        def inherited = false;
        for(nameOfInheritedDependency in inheritedDepNames){
            if(name.startsWith(nameOfInheritedDependency)){
                inherited = true
            }
        }
        if (declared && inherited){
            throw new IllegalArgumentException("Dependency can't be both declared and inherited!")
        }
        if(!declared && !inherited){
            transitive = true
        }
        def id = new ArtifactGroupAndId(depArt.groupId,depArt.artifactId);
        String resolutionTree = printer.printResolutionTreeForDependency(graph, id);
        int numberOfCandidates = graph.getAllCandidateVersionsForDependency(id).size()

        depsData.add(new Dep(artifactIdentifiers:depArt.artifactIdentifiers, fullname:name, updatable:false, declared:declared, inherited:inherited, transitive:transitive,
                resolutionTree:resolutionTree,
                numberOfCandidates:numberOfCandidates
        ))
    }
    updatableDeps.each{ name,version->
        for(dep in depsData){
            if(dep.fullname.startsWith(name)){
                dep.updatable = true;
                dep.latestVersion = version
            }
        }
    }
    return depsData
}