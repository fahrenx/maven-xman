package pl.tl.mavenxman.meter

import pl.tl.mavenxman.Dep

class HealthCheckMeter {

    static final double MAX_POINTS_FOR_SMALL_SIZE = 50;
    static final double MAX_POINTS_FOR_NO_CONFLICTS = 40;
    static final double MAX_POINTS_FOR_ALL_UPDATED = 10;

    int calculateHealtCheck(List<Dep> depList) {
        int sizePoints = calculateSizePoints(depList.size());
        int noConflictsPoints = calculateNoConflictsPoints(depList);
        int pointsForUpdates = calculatePointsForUpdates(depList.size());
        int pointsInTotal = sizePoints + noConflictsPoints + pointsForUpdates;
        return pointsInTotal
    }

    private int calculatePointsForUpdates(int numberOfUpdatable) {
        int points = MAX_POINTS_FOR_ALL_UPDATED - 0.2*MAX_POINTS_FOR_ALL_UPDATED;
        if(points <0) return 0
        else return points;
    }

    private int calculateNoConflictsPoints(List<Dep> deps) {
        //TODO what if omitted?  differentiate more accurately type of conflicts and seriousness
        int lightConflicts = depList.count { it.numberOfCandidates > 1 && it.numberOfCandidates <= 3 }
        int seriousConflicts = depList.count { it.numberOfCandidates > 3 && it.numberOfCandidates <= 6 }
        int extremeConflicts = depList.count { it.numberOfCandidates > 6 }
        if(extremeConflicts>0) return 0;
        int deductionForLightConflicts = lightConflicts*0.1*MAX_POINTS_FOR_NO_CONFLICTS;
        int deductionForSeriousConflicts = seriousConflicts * 0.2*MAX_POINTS_FOR_NO_CONFLICTS;
        int pointsForSize = MAX_POINTS_FOR_NO_CONFLICTS - deductionForLightConflicts -deductionForSeriousConflicts;
        if(pointsForSize < 0) return 0
        else return pointsForSize;
    }

    private int calculateSizePoints(int numberOfDeps) {

        if (numberOfDeps < 10) return MAX_POINTS_FOR_ALL_UPDATED - numberOfDeps * 0.01 * MAX_POINTS_FOR_SMALL_SIZE //90-100%
        else if (numberOfDeps >= 10 && numberOfDeps < 20) return 0.9 * MAX_POINTS_FOR_SMALL_SIZE - (numberOfDeps-10) * 0.02 * MAX_POINTS_FOR_SMALL_SIZE  //70-90%
        else if (numberOfDeps >= 20 && numberOfDeps < 50) return 0.7 * MAX_POINTS_FOR_SMALL_SIZE - (numberOfDeps-20) * 0.01 * MAX_POINTS_FOR_SMALL_SIZE  //40-70%
        else if (numberOfDeps >= 50 && numberOfDeps < 100) return 0.4 * MAX_POINTS_FOR_SMALL_SIZE - (numberOfDeps-50) * 0.05 * MAX_POINTS_FOR_SMALL_SIZE //15-40%
        else return 0;

    }

    class HealthCheck {
        int pointsForSmallSize;
        int pointsForNoConflicts;
        int pointsForUpdates;
        int totalPoints;
    }

}
