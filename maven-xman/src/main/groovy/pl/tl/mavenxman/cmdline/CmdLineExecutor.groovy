package pl.tl.mavenxman.cmdline

import org.apache.commons.exec.*

class CmdLineExecutor {

    public String runWithTimeout(String command, int timeoutInSecs) {
        if (timeoutInSecs <= 0) {
            throw new IllegalArgumentException("Timeout must be greater than zero!")
        }
        return execute(command, timeoutInSecs)
    }

    public String run(String command) {
        return execute(command, -1)
    }

    private String execute(String command, int timeoutInSecs) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        CommandLine commandline = CommandLine.parse(command);
        DefaultExecutor executor = new DefaultExecutor();
        PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
        executor.setStreamHandler(streamHandler);
        if (timeoutInSecs != -1) {
            ExecuteWatchdog watchdog = new ExecuteWatchdog(timeoutInSecs * 1000);
            executor.setWatchdog(watchdog);
        }
        executor.execute(commandline);
        return (outputStream.toString());

    }


}