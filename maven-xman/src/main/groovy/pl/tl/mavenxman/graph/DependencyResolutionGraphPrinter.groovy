package pl.tl.mavenxman.graph

import pl.tl.mavenxman.model.ArtifactGroupAndId
import pl.tl.mavenxman.model.DependencyTreeArtifact


class DependencyResolutionGraphPrinter {

    public String printResolutionTreeForDependency(DependencyResolutionGraph dependencyResolutionGraph, ArtifactGroupAndId artifactGroupAndId) {
        List<DependencyTreeArtifact> candidates = dependencyResolutionGraph.getAllCandidateVersionsForDependency(artifactGroupAndId);
        Set<DependencyTreeArtifact> artifactsToPrint = new LinkedHashSet<DependencyTreeArtifact>();
        for(DependencyTreeArtifact artifact in candidates){
            artifactsToPrint.addAll(getPathToRootAsSet(artifact));
        }
        StringBuilder sb = new StringBuilder();
        for(DependencyTreeArtifact artifact in artifactsToPrint){
            sb.append(artifact.getDescriptionInDependencyTree())
            sb.append("\n")
        }
        return sb.toString();
    }

    private Set<DependencyTreeArtifact> getPathToRootAsSet(DependencyTreeArtifact artifact) {
        List<DependencyTreeArtifact> path = [];
        DependencyTreeArtifact currentArtifact = artifact;
        while (currentArtifact != null) {
            path.add(currentArtifact)
            currentArtifact = currentArtifact.parent;
        }
        path = path.reverse();
        return new LinkedHashSet<DependencyTreeArtifact>(path);
    }
}

