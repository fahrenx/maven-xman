package pl.tl.mavenxman.graph
import pl.tl.mavenxman.model.ArtifactIdentifiers
import pl.tl.mavenxman.model.DependencyTreeArtifact
import groovy.transform.TupleConstructor

import java.util.regex.Matcher

class TextDependencyTreeParser {

    private NON_ROOT_ARTIFACT_REGEX = /(\||\s+)*([+\\]-\s+)(.*)/
    private ROOT_ARTIFACT_REGEX = /(.*):(.*):(.*):(.*)/


    DependencyResolutionGraph parse(String dependencyTree) {
        if(dependencyTree==null || dependencyTree.isEmpty()){
            throw new IllegalArgumentException("Cannot parse empty dependency tree!")
        }
        List<String> lines = dependencyTree.readLines();
        DependencyTreeArtifact rootArtifact = parseRootArtifact(lines.get(0))
        List<DependencyTreeArtifact> dependencies = [];
        Deque<LevelAndArtifact> artifactsLevels = [] as LinkedList;
        artifactsLevels.addFirst(new LevelAndArtifact(0,rootArtifact))
        if(lines.size()>1){
            DependencyTreeArtifact parent = rootArtifact
            lines[1..-1].each {
                DependencyTreeArtifact artifact = parseNonRootArtifact(it, parent, artifactsLevels)
                dependencies.add(artifact)
                parent = artifact.parent
            }
        }
        return DependencyResolutionGraph.createGraph(rootArtifact, dependencies);
    }
    boolean isNonRootArtifact(String line){
        def dependencyMatcher = (line =~ NON_ROOT_ARTIFACT_REGEX)
        return dependencyMatcher.matches()
    }

    private DependencyTreeArtifact parseNonRootArtifact(String line, DependencyTreeArtifact currentParent, Deque<LevelAndArtifact> artifactsLevels) {
        if(!isNonRootArtifact(line)){
            throw new IllegalArgumentException("Cannot parse line with artifact! line: '${line}'")
        }
        def matcher = (line =~ NON_ROOT_ARTIFACT_REGEX)
        def artifactDescription = matcher[0][3].toString().trim()
        def fullName = artifactDescription
        def resolved = isResolvedArtifact(artifactDescription);

        def artifactDescriptionWithoutBrackets = removeBracketsIfPresent(artifactDescription);
        String[] artifactDescriptionParts = artifactDescriptionWithoutBrackets.split(":")
        def groupId = artifactDescriptionParts[0]
        def artifactId = artifactDescriptionParts[1]
        def version = artifactDescriptionParts[3]

        // finding parent
        int level = calculateNestingLevel(matcher)
        DependencyTreeArtifact parent = findParentForCurrentArtifact(artifactId, level, currentParent, artifactsLevels)
        DependencyTreeArtifact artifact = new DependencyTreeArtifact(new ArtifactIdentifiers(groupId, artifactId, version), fullName, line, parent, resolved)
        artifactsLevels.addFirst(new LevelAndArtifact(level,artifact))
        return artifact

    }

    private boolean isResolvedArtifact(String artifactDescription) {
        return !artifactDescription.matches(/\(.*\)/)
    }
    private removeBracketsIfPresent(String str) {
        if(str.startsWith("(")){
            str = str[1..-1]
        }
        if(str.endsWith(")")){
            str = str[0..-2]
        }
        return str;
    }

    private DependencyTreeArtifact findParentForCurrentArtifact(artifactId, int level, DependencyTreeArtifact currentParent, Deque<LevelAndArtifact> artifactsLevels) {
        println "${artifactId} has level ${level}"
        DependencyTreeArtifact parent = currentParent
        if (level != artifactsLevels.getFirst().getLevel()) {
            parent = findParent(artifactsLevels, level)
        }
        parent
    }

    /**
     * find first artifact on the list which has lower level - it must be a parent
     */
    def findParent(Deque<LevelAndArtifact> levelAndArtifacts, int currentLevel) {
        for(entry in levelAndArtifacts){
            if(entry.level<currentLevel){
                return entry.dependencyTreeArtifact
            }
        }
    }

    /**
     * take the length part of the line which is before artifact information and divide it by 3
     */
    int calculateNestingLevel(Matcher nonRootArtifactMatcher) {
        int lengthOfThePartBeforeArtifactInformation = nonRootArtifactMatcher[0][0].toString().size() -nonRootArtifactMatcher[0][3].toString().size()
        return lengthOfThePartBeforeArtifactInformation/3;
    }

    private DependencyTreeArtifact parseRootArtifact(String line) {
        def matcher = (line =~ ROOT_ARTIFACT_REGEX)
        def fullName = matcher[0][0].toString().trim();
        def groupId = matcher[0][1].toString().trim()
        def artifactId = matcher[0][2].toString().trim()
        def version = matcher[0][4].toString().trim()
        return new DependencyTreeArtifact(new ArtifactIdentifiers(groupId, artifactId, version), fullName, line, null)
    }

    @TupleConstructor
    static class LevelAndArtifact{
        int level;
        DependencyTreeArtifact dependencyTreeArtifact;
    }


}
