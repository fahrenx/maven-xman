package pl.tl.mavenxman.graph

import pl.tl.mavenxman.model.ArtifactGroupAndId
import pl.tl.mavenxman.model.DependencyTreeArtifact

class DependencyResolutionGraph {

    final DependencyTreeArtifact rootArtifact;
    final List<DependencyTreeArtifact> dependencies
    final Map<ArtifactGroupAndId, List<DependencyTreeArtifact>> candidateVersionsForEachDependency;

    public static DependencyResolutionGraph createGraph(DependencyTreeArtifact rootArtifact, List<DependencyTreeArtifact> dependencies){
        Map<ArtifactGroupAndId, List<DependencyTreeArtifact>> candidateVersionsForEachDependency = new HashMap<String, List<DependencyTreeArtifact>>();
        for(DependencyTreeArtifact artifact in dependencies){
            ArtifactGroupAndId identifier = new ArtifactGroupAndId(artifact.getGroupId(), artifact.getArtifactId())
            List<DependencyTreeArtifact> resolutions = candidateVersionsForEachDependency.get(identifier);
            if(resolutions){
                resolutions.add(artifact)
            }else{
                candidateVersionsForEachDependency.put(identifier, [artifact])
            }
        }
        return new DependencyResolutionGraph(rootArtifact, dependencies, candidateVersionsForEachDependency);
    }

    private DependencyResolutionGraph(DependencyTreeArtifact rootArtifact, List<DependencyTreeArtifact> dependencies,
                                      Map<ArtifactGroupAndId, List<DependencyTreeArtifact>> candidateVersionsForEachDependency){
        this.rootArtifact = rootArtifact;
        this.dependencies = dependencies;
        this.candidateVersionsForEachDependency = candidateVersionsForEachDependency;
    }
    def List<DependencyTreeArtifact> getAllDependencies() {
        return dependencies;
    }

    def List<DependencyTreeArtifact> getAllCandidateVersionsForDependency(ArtifactGroupAndId artifactGroupAndId) {
        def candidates = candidateVersionsForEachDependency.get(artifactGroupAndId);
        return candidates ? candidates : new ArrayList<DependencyTreeArtifact>()
    }

    def getRootArtifact() {
        return rootArtifact;
    }

    def List<DependencyTreeArtifact> getResolvedDependencies() {
        return dependencies.findAll{it.resolved==true};
    }

    def List<DependencyTreeArtifact> getOmittedDependencies() {
        return dependencies.findAll{it.resolved==false};
    }
}
