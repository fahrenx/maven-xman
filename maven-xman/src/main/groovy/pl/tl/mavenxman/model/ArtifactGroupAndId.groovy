package pl.tl.mavenxman.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.TupleConstructor

@EqualsAndHashCode
@TupleConstructor
class ArtifactGroupAndId {

    final String groupId;
    final String artifactId;

}
