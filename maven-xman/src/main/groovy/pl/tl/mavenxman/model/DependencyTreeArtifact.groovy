package pl.tl.mavenxman.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@ToString(includeSuper=true)
@EqualsAndHashCode
class DependencyTreeArtifact extends Artifact{

    final String descriptionInDependencyTree;
    final DependencyTreeArtifact parent;
    final boolean resolved;

    public DependencyTreeArtifact(ArtifactIdentifiers artifactIdentifiers, String fullName, String descriptionInDependencyTree, DependencyTreeArtifact parent){
        this(artifactIdentifiers,fullName,descriptionInDependencyTree,parent,true)
    }

    public DependencyTreeArtifact(ArtifactIdentifiers artifactIdentifiers, String fullName, String descriptionInDependencyTree, DependencyTreeArtifact parent, boolean resolved){
        super(artifactIdentifiers, fullName)
        this.descriptionInDependencyTree = descriptionInDependencyTree;
        this.parent = parent;
        this.resolved = resolved;
    }
}
