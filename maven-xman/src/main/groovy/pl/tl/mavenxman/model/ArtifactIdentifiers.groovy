package pl.tl.mavenxman.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@ToString
@EqualsAndHashCode
class ArtifactIdentifiers {

    final String groupId;
    final String artifactId;
    final String version;

    public ArtifactIdentifiers(groupId, artifactId,version){
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
    }



}
