package pl.tl.mavenxman.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@ToString
@EqualsAndHashCode
class Artifact {

    final ArtifactIdentifiers artifactIdentifiers;
    final String fullName

    Artifact(ArtifactIdentifiers artifactIdentifiers, String fullName) {
        this.artifactIdentifiers = artifactIdentifiers;
        this.fullName = fullName;
    }

    String getGroupId(){
        artifactIdentifiers.getGroupId()
    }

    String getArtifactId(){
        artifactIdentifiers.getArtifactId()
    }

    String getVersion(){
        artifactIdentifiers.getVersion()
    }

}
