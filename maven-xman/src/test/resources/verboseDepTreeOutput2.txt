pl.tl:wf-core:jar:1.3-SNAPSHOT
+- org.mockito:mockito-all:jar:1.9.5:compile
+- org.easytesting:fest-assert:jar:1.4:compile
|  \- org.easytesting:fest-util:jar:1.1.6:compile
+- com.google.guava:guava:jar:15.0:compile
+- junit:junit:jar:4.10:compile
|  \- org.hamcrest:hamcrest-core:jar:1.1:compile
+- com.google.inject:guice:jar:3.0:compile
|  +- javax.inject:javax.inject:jar:1:compile
|  \- aopalliance:aopalliance:jar:1.0:compile
+- org.slf4j:slf4j-api:jar:1.7.5:compile
\- org.slf4j:slf4j-simple:jar:1.7.5:compile
   \- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for duplicate)