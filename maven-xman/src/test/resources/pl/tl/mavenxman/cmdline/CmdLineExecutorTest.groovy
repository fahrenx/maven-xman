package pl.tl.mavenxman.cmdline

import org.apache.commons.exec.ExecuteException
import spock.lang.Specification

class CmdLineExecutorTest extends Specification {

    def "when execute with timeout then method times out"() {
        when:
        new CmdLineExecutor().runWithTimeout("sleep 10", 1);
        then:
        ExecuteException ex = thrown()
        ex.message == 'Process exited with an error: 143 (Exit value: 143)'
    }

    def "when execute then output of command returned"() {
        when:
        String output = new CmdLineExecutor().run("printf \"Hello\"");
        then:
        output == "Hello";
    }


}
