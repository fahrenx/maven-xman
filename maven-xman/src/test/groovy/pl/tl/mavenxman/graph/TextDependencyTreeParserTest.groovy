package pl.tl.mavenxman.graph
import pl.tl.mavenxman.model.Artifact
import pl.tl.mavenxman.model.ArtifactGroupAndId
import pl.tl.mavenxman.model.ArtifactIdentifiers
import pl.tl.mavenxman.model.DependencyTreeArtifact
import pl.tl.mavenxman.graph.DependencyResolutionGraph
import pl.tl.mavenxman.graph.TextDependencyTreeParser
import spock.lang.Specification
//TODO separate test class with unit tests for parsing single lines
class TextDependencyTreeParserTest extends Specification{

    final static Artifact EXPECTED_ROOT_ARTIFACT = new DependencyTreeArtifact(
            new ArtifactIdentifiers("com.ebay.epd.falcons","harfile-extractor", "1.0-SNAPSHOT"),
            "com.ebay.epd.falcons:harfile-extractor:jar:1.0-SNAPSHOT", "com.ebay.epd.falcons:harfile-extractor:jar:1.0-SNAPSHOT", null)
    final static Artifact EXPECTED_FEST_DEPENDENCY_ARTIFACT = new DependencyTreeArtifact(
            new ArtifactIdentifiers("org.easytesting","fest-assert-core", "2.0M10"),
            "org.easytesting:fest-assert-core:jar:2.0M10:compile", "\\- org.easytesting:fest-assert-core:jar:2.0M10:compile", EXPECTED_ROOT_ARTIFACT);


    final static String TREE_ONLY_WITH_ROOT = "com.ebay.epd.falcons:harfile-extractor:jar:1.0-SNAPSHOT";
    final static String TREE_WITH_SINGLE_DEPENDENCY =
            "com.ebay.epd.falcons:harfile-extractor:jar:1.0-SNAPSHOT\n"+
            "\\- org.easytesting:fest-assert-core:jar:2.0M10:compile";
    final static String SAMPLE_TREE_WITH_CONFLICTS =
        "com.ebay.epd.falcons:harfile-extractor:jar:1.0-SNAPSHOT\n"+
        "+- org.slf4j:slf4j-simple:jar:1.7.5:compile\n" +
        "|  \\- (org.slf4j:slf4j-api:jar:1.7.5:compile - omitted for duplicate)\n" +
        "\\- net.lightbody.bmp:browsermob-proxy:jar:2.0-beta-9:compile\n" +
        "   \\- (org.slf4j:slf4j-api:jar:1.5.3:compile - omitted for conflict with 1.7.5)";

    private TextDependencyTreeParser parser

    def setup(){
        parser = new TextDependencyTreeParser();
    }

    def "if dependency tree contains only root artifact then list of dependencies is empty" () {
        when:
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(TREE_ONLY_WITH_ROOT)
        then:
            dependencyResolutionGraph.getAllDependencies().isEmpty() == true
    }

    def "if dependency tree contains only root artifact then root artifact is set and its identifiers are correctly extracted" () {
        when:
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(TREE_ONLY_WITH_ROOT)
        then:
            dependencyResolutionGraph.getRootArtifact() == EXPECTED_ROOT_ARTIFACT;
    }

    def "if dependency tree has one dependency then root artifact is set and its identifiers are correctly extracted" () {
        when:
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(TREE_WITH_SINGLE_DEPENDENCY)
        then:
            dependencyResolutionGraph.getRootArtifact() == EXPECTED_ROOT_ARTIFACT
    }

    def "if dependency tree has one dependency then this dependency is in all dependencies list with correctly set identifiers" () {
        when:
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(TREE_WITH_SINGLE_DEPENDENCY)
        then:
            dependencyResolutionGraph.getAllDependencies().size() == 1
            Artifact dependency = dependencyResolutionGraph.getAllDependencies().get(0);
            dependency.getGroupId() == "org.easytesting";
            dependency.getArtifactId() == "fest-assert-core";
            dependency.getVersion() == "2.0M10";
    }

    def "if dependency tree has one dependency then this dependency has correctly set parent" () {
        when:
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(TREE_WITH_SINGLE_DEPENDENCY)
        then:
            dependencyResolutionGraph.getAllDependencies().size() == 1
            Artifact dependency = dependencyResolutionGraph.getAllDependencies().get(0);
            dependency.getParent() == EXPECTED_ROOT_ARTIFACT
    }

    def "if dependency tree has one dependency then this dependency has correctly set full name and description in dependency tree" () {
        when:
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(TREE_WITH_SINGLE_DEPENDENCY)
        then:
            dependencyResolutionGraph.getAllDependencies().size() == 1
            Artifact dependency = dependencyResolutionGraph.getAllDependencies().get(0);
            dependency.getFullName() == "org.easytesting:fest-assert-core:jar:2.0M10:compile"
            dependency.getDescriptionInDependencyTree() == "\\- org.easytesting:fest-assert-core:jar:2.0M10:compile"
    }


    def "if sample dependency tree then parent relations correctly set" () {
        when:
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(SAMPLE_TREE_WITH_CONFLICTS)
        then:
            List<DependencyTreeArtifact> dependencies = dependencyResolutionGraph.getAllDependencies();
            dependencies.size() == 4
            assertCorrectArtifactIdAndParent(dependencies.get(0),"slf4j-simple", "harfile-extractor")
            assertCorrectArtifactIdAndParent(dependencies.get(1),"slf4j-api", "slf4j-simple")
            assertCorrectArtifactIdAndParent(dependencies.get(2),"browsermob-proxy", "harfile-extractor")
            assertCorrectArtifactIdAndParent(dependencies.get(3),"slf4j-api", "browsermob-proxy")
    }

    def "if sample big dependency tree then number of dependencies is correct" () {
        when:
            String depTree = this.getClass().getResource("/verboseDepTreeOutput.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(depTree)
        then:
            List<DependencyTreeArtifact> dependencies = dependencyResolutionGraph.getAllDependencies();
            dependencies.size() == 145
    }

    def "if sample big dependency tree then selected parent relations correctly set" () {
        when:
            String depTree = this.getClass().getResource("/verboseDepTreeOutput.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(depTree)
        then:
            List<DependencyTreeArtifact> dependencies = dependencyResolutionGraph.getAllDependencies();
            assertCorrectArtifactIdAndParent(dependencies.get(29),"sac", "cssparser")
            assertCorrectArtifactIdAndParent(dependencies.get(101),"jsoup", "sitebricks")
            assertCorrectArtifactIdAndParent(dependencies.get(124),"jetty-util", "jetty-io")
    }

    def "if sample dependency tree number 2 then number of dependencies is correct" () {
        when:
            String depTree = this.getClass().getResource("/verboseDepTreeOutput2.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(depTree)
        then:
            List<DependencyTreeArtifact> dependencies = dependencyResolutionGraph.getAllDependencies();
            dependencies.size() == 12
    }

    def "if sample dependency tree number 3 then number of dependencies is correct" () {
        when:
            String depTree = this.getClass().getResource("/verboseDepTreeOutput3.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(depTree)
        then:
            List<DependencyTreeArtifact> dependencies = dependencyResolutionGraph.getAllDependencies();
            dependencies.size() == 5
    }

    def "if igor-webapp dependency tree used then number of dependencies is correct" () {
        when:
            String depTree = this.getClass().getResource("/igor-webapp-dep.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(depTree)
        then:
            List<DependencyTreeArtifact> dependencies = dependencyResolutionGraph.getAllDependencies();
            dependencies.size() == 1575
    }


    def "if sample dependency tree number 2 then candidate versions lists for dependencies correctly calculated" () {
        when:
            String depTree = this.getClass().getResource("/verboseDepTreeOutput2.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(depTree)
        then:
            assertCandidateVersionsListAreCorrectForSample2(dependencyResolutionGraph)
    }

    def "if sample dependency tree number 2 then omitted dependencies correctly distinguished from resolved ones" () {
        when:
            String depTree = this.getClass().getResource("/verboseDepTreeOutput2.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = parser.parse(depTree)
        then:
            dependencyResolutionGraph.getAllDependencies().size() == 12;
            dependencyResolutionGraph.getResolvedDependencies().size() == 11;
            dependencyResolutionGraph.getOmittedDependencies().size() == 1;
    }

    private assertCandidateVersionsListAreCorrectForSample2(dependencyResolutionGraph){
        for(DependencyTreeArtifact artifact:dependencyResolutionGraph.getAllDependencies()){
            ArtifactGroupAndId id = new ArtifactGroupAndId(artifact.getGroupId(), artifact.getArtifactId())
            if(artifact.getArtifactId()!="slf4j-api"){
                dependencyResolutionGraph.getAllCandidateVersionsForDependency(id).size() == 1
                dependencyResolutionGraph.getAllCandidateVersionsForDependency(id).get(0) == artifact
            }
            else{
                dependencyResolutionGraph.getAllCandidateVersionsForDependency(id).size() == 2
            }
        }
        true
    }

    private def assertCorrectArtifactIdAndParent(DependencyTreeArtifact dependencyTreeArtifact, String artifactId, String parentArtifactId){
        dependencyTreeArtifact.getArtifactId() == artifactId
        dependencyTreeArtifact.getParent().getArtifactId() == parentArtifactId
    }





}
