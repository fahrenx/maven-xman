package pl.tl.mavenxman.graph

import pl.tl.mavenxman.model.ArtifactGroupAndId
import pl.tl.mavenxman.graph.DependencyResolutionGraph
import pl.tl.mavenxman.graph.DependencyResolutionGraphPrinter
import pl.tl.mavenxman.graph.TextDependencyTreeParser
import spock.lang.Specification


class DependencyResolutionGraphPrinterTest extends Specification {

    def "if sample big dependency tree then resolution tree for single artifact printed correctly" () {
        when:
            String depTree = this.getClass().getResource("/verboseDepTreeOutput.txt").text
            DependencyResolutionGraph dependencyResolutionGraph = new TextDependencyTreeParser().parse(depTree)
            String resolutionTree = new DependencyResolutionGraphPrinter().printResolutionTreeForDependency(dependencyResolutionGraph,
                    new ArtifactGroupAndId("commons-logging","commons-logging"))
        then:
            resolutionTree == ""
    }


}
