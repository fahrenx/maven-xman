package pl.tl.mavenxman

import spock.lang.Specification

class ConfigTest extends Specification{

    def "assigns correct pom.xml name consisting of parameter provided"() {
        when:
            def config = new Config(["/repo/test-app"].toArray())
        then:
            config.pomArg == "-f /repo/test-app/pom.xml"
    }
}
