package pl.tl.mavenxman.meter
import pl.tl.mavenxman.Dep
import spock.lang.Specification

class HealthCheckMeterTest extends Specification {

    def "empty list of dependencies is rated with maximum value" () {
        when:
            HealthCheckMeter healthCheckMeter = new HealthCheckMeter();
            HealthCheckMeter.HealthCheck healthCheck = healthCheckMeter.calculateHealtCheck(new LinkedList<Dep>())
        then:
            healthCheck.getPointsForNoConflicts() == HealthCheckMeter.MAX_POINTS_FOR_NO_CONFLICTS;
            healthCheck.getPointsForSmallSize() == HealthCheckMeter.MAX_POINTS_FOR_SMALL_SIZE;
            healthCheck.getPointsForUpdates() == HealthCheckMeter.MAX_POINTS_FOR_ALL_UPDATED;
            healthCheck.getPoin
    }

}
