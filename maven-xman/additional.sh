#!/bin/bash
pwd=`pwd`
path_to_script="${pwd}/scripts/"
export PATH=$PATH:${path_to_script}
export XMAN_HOME=pwd
echo "If you want to add xman permanently add to path add following line to your .bashrc or /etc/profile file:"
echo "export PATH=\$PATH:${path_to_script}"
echo "otherwise just invoke 'source install.sh'"
